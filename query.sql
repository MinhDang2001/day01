CREATE TABLE DMKHOA (
	MAKH Varchar(6),
	tenkhoa Varchar(30),
	PRIMARY KEY (MAKH)
); 

CREATE TABLE SINHVIEN (
	MaSV Varchar(6),
	HoSV Varchar(30),
	TenSV Varchar(30),
	GioiTinh Char(1),
	NgaySinh DateTime,
	NoiSinh Varchar(50),
	DiaChi Varchar(50),
	MaKH Varchar(6),
	HocBong int,
	PRIMARY KEY (MaSV)
); 


SELECT  *
FROM  SINHVIEN s
    LEFT JOIN DMKHOA d
    ON s.MaKH=d.MAKH WHERE d.tenkhoa="Cong nghe thong tin";

SELECT *
FROM SINHVIEN
WHERE SINHVIEN.MAKH IN (
    SELECT MaKH
    FROM DMKHOA
    WHERE tenkhoa = "Cong nghe thong tin");


